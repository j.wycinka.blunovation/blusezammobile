package com.blunovation.blusezam.Services.SOAPServices;

import android.os.AsyncTask;

import com.blunovation.blusezam.Consts;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SendReportFailuresDataService extends AsyncTask<String, Void, String> {
    private SendReportFailuresDataReceiver sendReportFailuresDataReceiver;

    public SendReportFailuresDataService(SendReportFailuresDataReceiver sendReportFailuresDataReceiver) {
        this.sendReportFailuresDataReceiver = sendReportFailuresDataReceiver;
    }

    @Override
    protected void onPostExecute(String s) {
        sendReportFailuresDataReceiver.onServiceExecuted(s);
    }

    @Override
    protected String doInBackground(String... params) {
        String METHOD_NAME = "sendReportFailuresData";
        String SOAP_ACTION = Consts.SOAP_NAMESPACE + METHOD_NAME;

        SoapObject soapObject = new SoapObject(Consts.SOAP_NAMESPACE, METHOD_NAME);

        PropertyInfo loginProperty = new PropertyInfo();
        loginProperty.setName("LOGIN");
        loginProperty.setValue(params[0]);
        loginProperty.setType(String.class);

        PropertyInfo passwordProperty = new PropertyInfo();
        passwordProperty.setName("PASSWORD");
        passwordProperty.setValue(params[1]);
        passwordProperty.setType(String.class);

        PropertyInfo jsonDataPrperty = new PropertyInfo();
        jsonDataPrperty.setName("DATA");
        jsonDataPrperty.setValue(params[2]);
        jsonDataPrperty.setType(String.class);

        soapObject.addProperty(loginProperty);
        soapObject.addProperty(passwordProperty);
        soapObject.addProperty(jsonDataPrperty);

        SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(soapObject);

        HttpTransportSE httpTransportSE = new HttpTransportSE(Consts.SOAP_URL, Consts.REQUEST_TIMEOUT);

        try {
            httpTransportSE.call(SOAP_ACTION, envelope);
            Object resposne = envelope.getResponse();
            return resposne.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface SendReportFailuresDataReceiver{
        void onServiceExecuted(String resposne);
    }
}
