package com.blunovation.blusezam.Models;

import java.util.ArrayList;

public class Machine {
    private int id;
    private String name;
    private String group;
    private String inventoryNumber;
    private int statusId;
    private String status;
    private ArrayList<MachineElement> elements;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<MachineElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<MachineElement> elements) {
        this.elements = elements;
    }
}
