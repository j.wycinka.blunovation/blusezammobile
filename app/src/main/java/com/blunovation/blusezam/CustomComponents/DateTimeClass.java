package com.blunovation.blusezam.CustomComponents;

import android.util.Log;

import java.util.Calendar;

public class DateTimeClass {
    private static final String TAG = "DataTimeClass";
    public int year, month, day, hour, minute;
    public Boolean Available;

    public DateTimeClass(){
        Available = true;
        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH) + 1;
        day = cal.get(Calendar.DAY_OF_MONTH);
        hour = cal.get(Calendar.HOUR);
        minute = cal.get(Calendar.MINUTE);
    }

    public String GetDateTimeAsString() {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day, hour, minute);
        Log.d(TAG, "year:"+year+", month:"+month+", day:"+ day+", hour:"+hour+", minute:"+minute+"." );
        return android.text.format.DateFormat.format("yyyy-MM-dd HH:mm", cal).toString();
    }
}
