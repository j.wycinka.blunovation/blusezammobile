package com.blunovation.blusezam.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class SendReportFailure {
    @SerializedName("failureType")
    private int failureTypeId;
    private int machineId;
    private int machineElementId;
    private Date failureDate;
    private String shortDescription;
    private String description;
    private String failureCode;
    private int priorityId;
    private String operations;

    public int getFailureTypeId() {
        return failureTypeId;
    }

    public void setFailureTypeId(int failureTypeId) {
        this.failureTypeId = failureTypeId;
    }

    public int getMachineId() {
        return machineId;
    }

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    public int getMachineElementId() {
        return machineElementId;
    }

    public void setMachineElementId(int machineElementId) {
        this.machineElementId = machineElementId;
    }

    public Date getFailureDate() {
        return failureDate;
    }

    public void setFailureDate(Date failureDate) {
        this.failureDate = failureDate;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFailureCode() {
        return failureCode;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    public String getOperations() {
        return operations;
    }

    public void setOperations(String operations) {
        this.operations = operations;
    }
}
