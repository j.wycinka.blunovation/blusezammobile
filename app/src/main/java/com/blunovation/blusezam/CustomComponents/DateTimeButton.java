package com.blunovation.blusezam.CustomComponents;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

public class DateTimeButton {
    private Context context;
    public DateTimeClass dateTime;
    public Button button;

    public DateTimeButton(Context context, DateTimeClass dateTime, Button button) {
        this.context = context;
        this.dateTime = dateTime;
        this.button = button;
        this.AttachDateTimePicker();
    }

    private void AttachDateTimePicker() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewButton) {
                dateTime.Available = true;
                Calendar c = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker viewDatePicker, int year, int month, int dayOfMonth) {
                                if (dateTime.Available) {
                                    dateTime.Available = false;
                                    dateTime.year = year;
                                    dateTime.month = month + 1;
                                    dateTime.day = dayOfMonth;

                                    Calendar c = Calendar.getInstance();

                                    TimePickerDialog timePickerDialog = new TimePickerDialog(
                                            context,
                                            new TimePickerDialog.OnTimeSetListener() {
                                                @Override
                                                public void onTimeSet(TimePicker viewTimePicker, int hourOfDay, int minute) {
                                                    dateTime.hour = hourOfDay;
                                                    dateTime.minute = minute;

                                                    button.setText(dateTime.GetDateTimeAsString());
                                                }
                                            },
                                            c.get(Calendar.HOUR),
                                            c.get(Calendar.MINUTE),
                                            true);
                                    timePickerDialog.show();
                                }
                            }
                        },
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }
}
