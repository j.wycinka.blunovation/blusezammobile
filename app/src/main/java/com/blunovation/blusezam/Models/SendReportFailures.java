package com.blunovation.blusezam.Models;

import java.util.ArrayList;

public class SendReportFailures {
    private ArrayList<SendReportFailure> reportFailures;

    public ArrayList<SendReportFailure> getReportFailures() {
        return reportFailures;
    }

    public void setReportFailures(ArrayList<SendReportFailure> reportFailures) {
        this.reportFailures = reportFailures;
    }
}
