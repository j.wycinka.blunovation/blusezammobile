package com.blunovation.blusezam.Activities.CrashActivities;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.blunovation.blusezam.CustomComponents.DateTimeButton;
import com.blunovation.blusezam.CustomComponents.DateTimeClass;
import com.blunovation.blusezam.R;

public class AddCrashActivity extends Activity implements View.OnClickListener {

    Button failOccurredAt_btn;
    DateTimeClass OccurredAt_dt;
    DateTimeButton OccurredAt_dateTime;

    Button repairBeginAt_btn;
    DateTimeClass repairBeginAt_dt;
    DateTimeButton RepairBeginAt_dateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_crash);

        Button button = findViewById(R.id.submitFaultButton);
        button.setOnClickListener(this);

        OccurredAt_dt = new DateTimeClass();
        failOccurredAt_btn = findViewById(R.id.failOccurredAt_btn);
        OccurredAt_dateTime = new DateTimeButton(this, OccurredAt_dt, failOccurredAt_btn);

        repairBeginAt_dt = new DateTimeClass();
        repairBeginAt_btn = findViewById(R.id.repairBeginAt_btn);
        RepairBeginAt_dateTime = new DateTimeButton(this, repairBeginAt_dt, repairBeginAt_btn);
    }

    @Override
    public void onClick(View v) {
        postFault();
    }

    private void postFault(){
        Toast.makeText(this,"Posting fault",Toast.LENGTH_LONG).show();
    }
}
