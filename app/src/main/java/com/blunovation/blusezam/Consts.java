package com.blunovation.blusezam;

public class Consts {
    public static final String SOAP_URL = "http://serwer.blunovation.com:81/blusezamcmms/index.php?r=soap/quote&ws=1";
    public static final String SOAP_NAMESPACE = "http://serwer.blunovation.com:81/blusezamcmms/";
    public static final String SHARED_PREF_NAME = "BluSezam";

    public static final String LOGIN_KEY = "LOGIN";
    public static final String PASSWORD_KEY = "PASSWORD";

    public static int REQUEST_TIMEOUT = 7000;

    public static String LOGIN;
    public static String PASSWORD;
}
