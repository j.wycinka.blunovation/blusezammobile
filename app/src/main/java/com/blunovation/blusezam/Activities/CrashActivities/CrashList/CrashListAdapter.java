package com.blunovation.blusezam.Activities.CrashActivities.CrashList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blunovation.blusezam.Models.Machine;
import com.blunovation.blusezam.R;

import java.util.ArrayList;

public class CrashListAdapter extends RecyclerView.Adapter<CrashListAdapter.MyViewHolder> {
    private ArrayList<Machine> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public MyViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.machineNameTextView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CrashListAdapter(ArrayList<Machine> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crash_list_row, parent, false);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = ((RecyclerView)parent).getChildAdapterPosition(v);
                mDataset.remove(i);
                notifyDataSetChanged();
            }
        });

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Machine machine = mDataset.get(position);

        holder.mTextView.setText(machine.getName());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}