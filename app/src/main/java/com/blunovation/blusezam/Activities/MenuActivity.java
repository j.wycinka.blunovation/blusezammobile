package com.blunovation.blusezam.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.blunovation.blusezam.Activities.CrashActivities.AddCrashActivity;
import com.blunovation.blusezam.Consts;
import com.blunovation.blusezam.R;
import com.blunovation.blusezam.Activities.CrashActivities.CrashList.CrashListActivity;

public class MenuActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button addNewCrashButton = findViewById(R.id.addNewCrashButton);
        Button faultsAndAccidentsListButton = findViewById(R.id.goToCrashListButton);
        Button logoutButton = findViewById(R.id.logoutButton);

        addNewCrashButton.setOnClickListener(this);
        faultsAndAccidentsListButton.setOnClickListener(this);
        logoutButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int clickedId = v.getId();
        Intent intent = null;

        switch(clickedId)
        {
            case R.id.addNewCrashButton:
                intent = new Intent(MenuActivity.this, AddCrashActivity.class);
                break;
            case R.id.goToCrashListButton:
                intent = new Intent(MenuActivity.this, CrashListActivity.class);
                break;
            case R.id.logoutButton:
                removeStoredCredentials();
                intent = new Intent(MenuActivity.this, LoginActivity.class);
                break;
        }

        if(intent != null){
            startActivity(intent);
        }
    }

    private void removeStoredCredentials(){
        SharedPreferences.Editor editor = getSharedPreferences(Consts.SHARED_PREF_NAME, MODE_PRIVATE).edit();
        editor.remove(Consts.LOGIN_KEY);
        editor.remove(Consts.PASSWORD_KEY);
        editor.apply();
    }
}
