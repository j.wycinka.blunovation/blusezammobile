package com.blunovation.blusezam.Activities.CrashActivities.CrashList;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.blunovation.blusezam.Consts;
import com.blunovation.blusezam.Models.Machine;
import com.blunovation.blusezam.R;
import com.blunovation.blusezam.Services.SOAPServices.GetMachinesDataService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CrashListActivity extends Activity implements GetMachinesDataService.GetMachinesDataReceiver{

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new GetMachinesDataService(this).execute(Consts.LOGIN,Consts.PASSWORD);

        setContentView(R.layout.activity_crash_list);
        mRecyclerView = findViewById(R.id.crashListRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private ArrayList<Machine> getMachinesFrom(String rawJson){
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        try{
            Machine[] machines = gson.fromJson(rawJson, Machine[].class);

            List<Machine> machinesList = Arrays.asList(machines);

            return new ArrayList<>(machinesList);

        } catch(Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void onServiceExecuted(String resposne) {

        if(resposne == null){
            Toast.makeText(this, getString(R.string.connectivity_problem), Toast.LENGTH_LONG).show();
        } else {
            ArrayList<Machine> machines = getMachinesFrom(resposne);
            mRecyclerView.setAdapter(new CrashListAdapter(machines));
        }
    }
}
