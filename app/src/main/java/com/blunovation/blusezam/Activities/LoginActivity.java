package com.blunovation.blusezam.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.blunovation.blusezam.Consts;
import com.blunovation.blusezam.R;
import com.blunovation.blusezam.Services.SOAPServices.AuthenticationCheckService;

public class LoginActivity extends Activity implements View.OnClickListener, AuthenticationCheckService.AuthenticationCheckReceiver{
    private TextInputLayout loginLayout;
    private TextInputLayout passwordLayout;
    private Button signinButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginLayout = findViewById(R.id.loginTextInputLayout);
        passwordLayout = findViewById(R.id.passwordTextInputLayout);

        signinButton = findViewById(R.id.signInButton);
        signinButton.setOnClickListener(this);

        if(areCredentialsStored()){
            setEditTextsWithCredentials();
            goToMenuActivity();
        }
    }

    @Override
    public void onClick(View v) {
        hideVirtualKeyboard(v);
        Consts.LOGIN = loginLayout.getEditText().getText().toString();
        Consts.PASSWORD = passwordLayout.getEditText().getText().toString();

        loginLayout.setErrorEnabled(false);
        passwordLayout.setErrorEnabled(false);

        loginLayout.getEditText().setEnabled(false);
        passwordLayout.getEditText().setEnabled(false);
        signinButton.setEnabled(false);

        new AuthenticationCheckService(this).execute(Consts.LOGIN, Consts.PASSWORD);
    }

    @Override
    public void onServiceExecuted(String resposne) {
        loginLayout.getEditText().setEnabled(true);
        passwordLayout.getEditText().setEnabled(true);
        signinButton.setEnabled(true);

        int authStatus = Integer.parseInt(resposne);
        boolean areCredentialsCorrect = authStatus == 1;

        if(resposne != null && areCredentialsCorrect){
            storeCredetials();
            goToMenuActivity();
        } else if (resposne == null){
            Toast.makeText(this, getString(R.string.connectivity_problem), Toast.LENGTH_LONG).show();
        } else{
            loginLayout.setError(getString(R.string.incorrect_login_error_message));
            passwordLayout.setError(getString(R.string.incorrect_password_error_message));
        }
    }

    private void storeCredetials(){
        SharedPreferences.Editor editor = getSharedPreferences(Consts.SHARED_PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(Consts.LOGIN_KEY, Consts.LOGIN);
        editor.putString(Consts.PASSWORD_KEY, Consts.PASSWORD);
        editor.apply();
    }

    private void hideVirtualKeyboard(View v){
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputManager != null) {
            inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean areCredentialsStored(){
        SharedPreferences prefs = getSharedPreferences(Consts.SHARED_PREF_NAME, MODE_PRIVATE);
        return prefs.contains(Consts.LOGIN_KEY) && prefs.contains(Consts.PASSWORD_KEY);
    }

    private void setEditTextsWithCredentials(){
        SharedPreferences prefs = getSharedPreferences(Consts.SHARED_PREF_NAME, MODE_PRIVATE);
        String restoredLogin = prefs.getString(Consts.LOGIN, null);
        String restoredPassword = prefs.getString(Consts.PASSWORD, null);
        if (restoredLogin != null && restoredPassword != null) {
            loginLayout.getEditText().setText(restoredLogin);
            passwordLayout.getEditText().setText(restoredPassword);
        }
    }

    private void goToMenuActivity(){
        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
        startActivity(intent);
    }
}
